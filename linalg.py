#!/usr/bin/env python
import copy
import cProfile


class Matrix:
    def __init__(self, matrix=[]):
        # self.matrix = self.flatten(matrix)
        self.matrix = matrix
        self.rows = len(matrix)
        self.cols = len(matrix[0])

    # PRIMITIVE OPERATIONS

    def __add__(self, other):
        '''
        Matrix + Matrix -> Matrix
        '''
        # Not checking for same dimensions returns incorrect result whenever the self matrix is smaller than other
        # because it won't raise indexerror; matrices of different size will be added without error.
        if self.rows == other.rows and self.cols == other.cols:
            m = copy.deepcopy(self.matrix)
            for i in range(self.rows):
                for j in range(self.cols):
                    m[i][j] += other.matrix[i][j]
            return Matrix(m)
        else:
            raise UndefinedOperation("Matrices are of different size")

    def __sub__(self, other):
        '''
        Matrix - Matrix -> Matrix
        '''
        if self.rows == other.rows and self.cols == other.cols:
            m = copy.deepcopy(self.matrix)
            for i in range(self.rows):
                for j in range(self.cols):
                    m[i][j] -= other.matrix[i][j]
            return Matrix(m)
        else:
            raise UndefinedOperation("Matrices are of different size")

    def __mul__(self, other):
        '''
        Matrix * Matrix -> Matrix ;
        Matrix * constant -> Matrix

        ex:
        (self) * (other)
        2 x 4  *  4 x 3  ->  self.rows x other.cols  (2 x 3)
        rows x columns
        '''
        if type(self) is Matrix and type(other) is Matrix:  # matrix mult
            if self.cols == other.rows:
                m = [[0 for i in range(other.cols)] for j in range(self.rows)]
                for i in range(self.rows):
                    for j in range(other.cols):
                        # sum(i*j for i, j in zip(a,b)) where a and b are lists
                        # from http://stackoverflow.com/a/961327
                        m[i][j] = sum(a * b for a, b in zip(self.matrix[i], other.transpose().matrix[j]))
                        # get the sum of the multiples of the row of self and the col of other,
                        # which just turns out to be the row of the transpose of other
                return Matrix(m)
            else:
                raise UndefinedOperation("Column and row size do not correspond")
        else:                                               # scalar mult
            m = copy.deepcopy(self.matrix)
            for i in range(self.rows):
                for j in range(self.cols):
                    m[i][j] *= other
            return Matrix(m)

    def __rmul__(self, other):  # only need to handle scalar mult
        '''
        constant * Matrix -> Matrix
        '''
        m = copy.deepcopy(self.matrix)
        for i in range(self.rows):
            for j in range(self.cols):
                m[i][j] *= other
        return Matrix(m)

    # ADVANCED OPERATIONS

    @staticmethod
    def flatten(matrix):
        '''
        flatten : matrix -> list
        '''
        return [x for i in matrix for x in i]

    def trace(self):
        '''
        trace : none -> number
        '''
        trace = 0
        if self.rows == self.cols:  # if matrix is square
            for i in range(self.rows):
                trace += self.matrix[i][i]  # same index for i, j
            return trace
        else:
            raise UndefinedOperation("Matrix is not square")

    def transpose(self):
        '''
        transpose : none -> Matrix
        '''
        """ My original working code which is ~ 2x slower:
        t = [[0 for i in range(self.rows)] for j in range(self.cols)]  # reversed dimensions
        for i in range(self.cols):  # reversed iteration order
            for j in range(self.rows):
                t[i][j] = self.matrix[j][i]
        return Matrix(t)
        """
        # from http://stackoverflow.com/a/2921721
        # zip returns tuples, so list(tup) deals with that
        return Matrix([list(tup) for tup in zip(*self.matrix)])

    def inverse(self):
        pass

    def det(self):
        '''
        det : none -> int
        '''
        # if self.rows == self.cols:

    # VECTOR OPERATIONS

    @staticmethod
    def dot(v1, v2):
        return sum([a * b for a, b in zip(Matrix.flatten(v1), Matrix.flatten(v2))])


class UndefinedOperation(BaseException):
    pass


def main():
    a = [
        [2, 4, 5, 7, 4, 6, 4, 2, 4, 7],
        [1, 2, 7, 4, 7, 4, 6, 3, 6, 8],
        [7, 5, 3, 6, 4, 2, 4, 6, 7, 3],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4],
        [5, 3, 1, 5, 7, 4, 2, 5, 7, 4]]

    am = Matrix(a)
    b = copy.deepcopy(am)

    for t in range(1000):
        b *= am

    print(Matrix.dot(Matrix([[1, 2, 3]]).matrix, Matrix([[1, 2, 3]]).matrix))  # ugly, but I maintain the standard of [[]]
    print( (Matrix( [[1, 2, 4], [2, 6, 0]] ) * Matrix( [[1], [-1], [7]] )).matrix )


cProfile.run('main()')
